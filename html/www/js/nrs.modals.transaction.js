/******************************************************************************
 * Copyright © 2013-2016 The Nxt Core Developers.                             *
 * Copyright © 2016-2018 Jelurida IP B.V.                                     *
 *                                                                            *
 * See the LICENSE.txt file at the top-level directory of this distribution   *
 * for licensing information.                                                 *
 *                                                                            *
 * Unless otherwise agreed in a custom licensing agreement with Jelurida B.V.,*
 * no part of the Nxt software, including this file, may be copied, modified, *
 * propagated, or distributed except according to the terms contained in the  *
 * LICENSE.txt file.                                                          *
 *                                                                            *
 * Removal or modification of this copyright notice is prohibited.            *
 *                                                                            *
 ******************************************************************************/

/**
 * @depends {nrs.js}
 * @depends {nrs.modals.js}
 */
var NRS = (function (NRS, $, undefined) {
    $('body').on("click", ".show_transaction_modal_action", function (e) {
        e.preventDefault();

        var transactionId = $(this).data("transaction");
        var sharedKey = $(this).data("sharedkey");
        var infoModal = $('#transaction_info_modal');
        var isModalVisible = false;
        if (infoModal && infoModal.data('bs.modal')) {
            isModalVisible = infoModal.data('bs.modal').isShown;
        }
        if ($(this).data("back") == "true") {
            NRS.modalStack.pop(); // The forward modal
            NRS.modalStack.pop(); // the current modal
        }
        NRS.showTransactionModal(transactionId, isModalVisible, sharedKey);
    });

    NRS.showTransactionModal = function (transaction, isModalVisible, sharedKey) {
        if (NRS.fetchingModalData) {
            return;
        }

        NRS.fetchingModalData = true;

        $("#transaction_info_output_top, #transaction_info_output_bottom, #transaction_info_bottom").html("").hide();
        $("#transaction_info_callout").hide();
        var infoTable = $("#transaction_info_table");
        infoTable.hide();
        infoTable.find("tbody").empty();

        try {
            if (typeof transaction != "object") {
                NRS.sendRequest("getTransaction", {
                    "transaction": transaction,
                    "includePhasingResult": true
                }, function (response, input) {
                    response.transaction = input.transaction;
                    NRS.processTransactionModalData(response, isModalVisible, sharedKey);
                });
            } else {
                NRS.processTransactionModalData(transaction, isModalVisible, sharedKey);
            }
        } catch (e) {
            NRS.fetchingModalData = false;
            throw e;
        }
    };

    NRS.getPhasingDetails = function(phasingDetails, phasingParams) {
        var votingModel = NRS.getVotingModelName(parseInt(phasingParams.phasingVotingModel));
        phasingDetails.votingModel = $.t(votingModel);
        switch (votingModel) {
            case 'ASSET':
                NRS.sendRequest("getAsset", { "asset": phasingParams.phasingHolding }, function(response) {
                    phasingDetails.quorum = NRS.convertToQNTf(phasingParams.phasingQuorum, response.decimals);
                    phasingDetails.minBalance = NRS.convertToQNTf(phasingParams.phasingMinBalance, response.decimals);
                }, { isAsync: false });
                break;
            case 'CURRENCY':
                NRS.sendRequest("getCurrency", { "currency": phasingParams.phasingHolding }, function(response) {
                    phasingDetails.quorum = NRS.convertToQNTf(phasingParams.phasingQuorum, response.decimals);
                    phasingDetails.minBalance = NRS.convertToQNTf(phasingParams.phasingMinBalance, response.decimals);
                }, { isAsync: false });
                break;
            default:
                phasingDetails.quorum = phasingParams.phasingQuorum;
                phasingDetails.minBalance = phasingParams.phasingMinBalance;
        }
        var phasingTransactionLink = NRS.getTransactionLink(phasingParams.phasingHolding);
        if (NRS.constants.VOTING_MODELS[votingModel] == NRS.constants.VOTING_MODELS.ASSET) {
            phasingDetails.asset_formatted_html = phasingTransactionLink;
        } else if (NRS.constants.VOTING_MODELS[votingModel] == NRS.constants.VOTING_MODELS.CURRENCY) {
            phasingDetails.currency_formatted_html = phasingTransactionLink;
        }
        var minBalanceModel = NRS.getMinBalanceModelName(parseInt(phasingParams.phasingMinBalanceModel));
        phasingDetails.minBalanceModel = $.t(minBalanceModel);
        var rows = "";
        if (phasingParams.phasingWhitelist && phasingParams.phasingWhitelist.length > 0) {
            rows = "<table class='table table-striped'><thead><tr>" +
                "<th>" + $.t("Account") + "</th>" +
                "</tr></thead><tbody>";
            for (var i = 0; i < phasingParams.phasingWhitelist.length; i++) {
                var account = NRS.convertNumericToRSAccountFormat(phasingParams.phasingWhitelist[i]);
                rows += "<tr><td><a href='#' data-user='" + NRS.escapeRespStr(account) + "' class='show_account_modal_action'>" + NRS.getAccountTitle(account) + "</a></td></tr>";
            }
            rows += "</tbody></table>";
        } else {
            rows = "-";
        }
        phasingDetails.whitelist_formatted_html = rows;
        if (phasingParams.phasingLinkedFullHashes && phasingParams.phasingLinkedFullHashes.length > 0) {
            rows = "<table class='table table-striped'><tbody>";
            for (i = 0; i < phasingParams.phasingLinkedFullHashes.length; i++) {
                rows += "<tr><td>" + phasingParams.phasingLinkedFullHashes[i] + "</td></tr>";
            }
            rows += "</tbody></table>";
        } else {
            rows = "-";
        }
        phasingDetails.full_hash_formatted_html = rows;
        if (phasingParams.phasingHashedSecret) {
            phasingDetails.hashedSecret = phasingParams.phasingHashedSecret;
            phasingDetails.hashAlgorithm = NRS.getHashAlgorithm(phasingParams.phasingHashedSecretAlgorithm);
        }
    };

    NRS.processTransactionModalData = function (transaction, isModalVisible, sharedKey) {
        NRS.setBackLink();
        NRS.modalStack.push({ class: "show_transaction_modal_action", key: "transaction", value: transaction.transaction });
        try {
            var async = false;

            var transactionDetails = $.extend({}, transaction);
            delete transactionDetails.attachment;
            if (transactionDetails.referencedTransaction == "0") {
                delete transactionDetails.referencedTransaction;
            }
            delete transactionDetails.transaction;

            if (!transactionDetails.confirmations) {
                transactionDetails.confirmations = "/";
            }
            if (!transactionDetails.block) {
                transactionDetails.block = "unconfirmed";
            }
            if (transactionDetails.timestamp) {
                transactionDetails.transactionTime = NRS.formatTimestamp(transactionDetails.timestamp);
            }
            if (transactionDetails.blockTimestamp) {
                transactionDetails.blockGenerationTime = NRS.formatTimestamp(transactionDetails.blockTimestamp);
            }
            if (transactionDetails.height == NRS.constants.MAX_INT_JAVA) {
                transactionDetails.height = "unknown";
            } else {
                transactionDetails.height_formatted_html = NRS.getBlockLink(transactionDetails.height);
                delete transactionDetails.height;
            }
            if (transactionDetails.executionHeight !== undefined) {
                transactionDetails.execution_height_formatted_html = NRS.getBlockLink(transactionDetails.executionHeight);
                delete transactionDetails.executionHeight;
            }
            $("#transaction_info_modal_transaction").html(NRS.escapeRespStr(transaction.transaction));

            $("#transaction_info_tab_link").tab("show");

            $("#transaction_info_details_table").find("tbody").empty().append(NRS.createInfoTable(transactionDetails, true));
            var infoTable = $("#transaction_info_table");
            infoTable.find("tbody").empty();

            var incorrect = false;
            if (transaction.senderRS == NRS.accountRS) {
                $("#transaction_info_modal_send_money").attr('disabled','disabled');
                $("#transaction_info_modal_transfer_currency").attr('disabled','disabled');
                $("#transaction_info_modal_send_message").attr('disabled','disabled');
            } else {
                $("#transaction_info_modal_send_money").removeAttr('disabled');
                $("#transaction_info_modal_transfer_currency").removeAttr('disabled');
                $("#transaction_info_modal_send_message").removeAttr('disabled');
            }
            var accountButton;
            if (transaction.senderRS in NRS.contacts) {
                accountButton = NRS.contacts[transaction.senderRS].name.escapeHTML();
                $("#transaction_info_modal_add_as_contact").attr('disabled','disabled');
            } else {
                accountButton = transaction.senderRS;
                $("#transaction_info_modal_add_as_contact").removeAttr('disabled');
            }
            var approveTransactionButton = $("#transaction_info_modal_approve_transaction");
            if (!transaction.attachment || !transaction.block ||
                !transaction.attachment.phasingFinishHeight ||
                transaction.attachment.phasingFinishHeight <= NRS.lastBlockHeight) {
                approveTransactionButton.attr('disabled', 'disabled');
            } else {
                approveTransactionButton.removeAttr('disabled');
                approveTransactionButton.data("transaction", transaction.transaction);
                approveTransactionButton.data("fullhash", transaction.fullHash);
                approveTransactionButton.data("timestamp", transaction.timestamp);
                approveTransactionButton.data("minBalanceFormatted", "");
                approveTransactionButton.data("votingmodel", transaction.attachment.phasingVotingModel);
            }
            var extendDataButton = $("#transaction_info_modal_extend_data");
            /*if (transaction.type == NRS.subtype.TaggedDataUpload.type && transaction.subtype == NRS.subtype.TaggedDataUpload.subtype) {
                extendDataButton.removeAttr('disabled');
                extendDataButton.data("transaction", transaction.transaction);
            } else*/ {
                extendDataButton.attr('disabled','disabled');
            }

            $("#transaction_info_actions").show();
            $("#transaction_info_actions_tab").find("button").data("account", accountButton);

            if (transaction.attachment && transaction.attachment.phasingFinishHeight) {
                var finishHeight = transaction.attachment.phasingFinishHeight;
                var phasingDetails = {};
                phasingDetails.finishHeight = finishHeight;
                phasingDetails.finishIn = ((finishHeight - NRS.lastBlockHeight) > 0) ? (finishHeight - NRS.lastBlockHeight) + " " + $.t("blocks") : $.t("finished");
                NRS.getPhasingDetails(phasingDetails, transaction.attachment);
                $("#phasing_info_details_table").find("tbody").empty().append(NRS.createInfoTable(phasingDetails, true));
                $("#phasing_info_details_link").show();
            } else {
                $("#phasing_info_details_link").hide();
            }
            // TODO Someday I'd like to replace it with if (NRS.isOfType(transaction, "OrdinaryPayment"))
            var data;
            var message;
            var fieldsToDecrypt = {};
            var i;
            if (transaction.type == 0) {
                switch (transaction.subtype) {
                    case 0:
                        data = {
                            "type": $.t("ordinary_payment"),
                            "amount": transaction.amountNQT,
                            "fee": transaction.feeNQT,
                            "recipient": transaction.recipientRS ? transaction.recipientRS : transaction.recipient,
                            "sender": transaction.senderRS ? transaction.senderRS : transaction.sender
                        };

                        infoTable.find("tbody").append(NRS.createInfoTable(data));
                        infoTable.show();

                        break;
                    default:
                        incorrect = true;
                        break;
                }
            } else if (transaction.type == 1) {
                switch (transaction.subtype) {
                    case 0:
                        var $output = $("#transaction_info_output_top");
                        if (transaction.attachment) {
                            if (transaction.attachment.message) {
                                if (!transaction.attachment["version.Message"] && !transaction.attachment["version.PrunablePlainMessage"]) {
                                    try {
                                        message = converters.hexStringToString(transaction.attachment.message);
                                    } catch (err) {
                                        //legacy
                                        if (transaction.attachment.message.indexOf("feff") === 0) {
                                            message = NRS.convertFromHex16(transaction.attachment.message);
                                        } else {
                                            message = NRS.convertFromHex8(transaction.attachment.message);
                                        }
                                    }
                                } else {
                                    if (transaction.attachment.messageIsText) {
                                        message = String(transaction.attachment.message);
                                    } else {
                                        message = $.t("binary_data");
                                    }
                                }
                                $output.html("<div style='color:#999999;padding-bottom:10px'><i class='fa fa-unlock'></i> " + $.t("public_message") + "</div><div style='padding-bottom:10px'>" + NRS.escapeRespStr(message).nl2br() + "</div>");
                            }

                            if (transaction.attachment.encryptedMessage || (transaction.attachment.encryptToSelfMessage && NRS.account == transaction.sender)) {
                                $output.append("" +
                                    "<div id='transaction_info_decryption_form'></div>" +
                                    "<div id='transaction_info_decryption_output' style='display:none;padding-bottom:10px;'></div>"
                                );
                                if (transaction.attachment.encryptedMessage) {
                                    fieldsToDecrypt.encryptedMessage = $.t("encrypted_message");
                                }
                                if (transaction.attachment.encryptToSelfMessage && NRS.account == transaction.sender) {
                                    fieldsToDecrypt.encryptToSelfMessage = $.t("note_to_self");
                                }
                                var options = {
                                    "noPadding": true,
                                    "formEl": "#transaction_info_decryption_form",
                                    "outputEl": "#transaction_info_decryption_output"
                                };
                                if (sharedKey) {
                                    options["sharedKey"] = sharedKey;
                                }
                                NRS.tryToDecrypt(transaction, fieldsToDecrypt, NRS.getAccountForDecryption(transaction), options);
                            }
                        } else {
                            $output.append("<div style='padding-bottom:10px'>" + $.t("message_empty") + "</div>");
                        }
                        var isCompressed = false;
                        if (transaction.attachment.encryptedMessage) {
                            isCompressed = transaction.attachment.encryptedMessage.isCompressed;
                        } else if (transaction.attachment.encryptToSelfMessage) {
                            isCompressed = transaction.attachment.encryptToSelfMessage.isCompressed;
                        }
                        var hash = transaction.attachment.messageHash || transaction.attachment.encryptedMessageHash;
                        var hashRow = hash ? ("<tr><td><strong>" + $.t("hash") + "</strong>:&nbsp;</td><td>" + hash + "</td></tr>") : "";
                        var downloadLink = "";
                        if (transaction.attachment.messageHash && !NRS.isTextMessage(transaction) && transaction.block) {
                            downloadLink = "<tr><td>" + NRS.getMessageDownloadLink(transaction.transaction, sharedKey) + "</td></tr>";
                        }
                        $output.append("<table>" +
                            "<tr><td><strong>" + $.t("from") + "</strong>:&nbsp;</td><td>" + NRS.getAccountLink(transaction, "sender") + "</td></tr>" +
                            "<tr><td><strong>" + $.t("to") + "</strong>:&nbsp;</td><td>" + NRS.getAccountLink(transaction, "recipient") + "</td></tr>" +
                            "<tr><td><strong>" + $.t("compressed") + "</strong>:&nbsp;</td><td>" + isCompressed + "</td></tr>" +
                            hashRow + downloadLink +
                        "</table>");
                        $output.show();
                        break;
                    case 1:
                        data = {
                            "type": $.t("alias_assignment"),
                            "alias": transaction.attachment.alias,
                            "data_formatted_html": transaction.attachment.uri.autoLink()
                        };
                        data["sender"] = transaction.senderRS ? transaction.senderRS : transaction.sender;
                        infoTable.find("tbody").append(NRS.createInfoTable(data));
                        infoTable.show();

                        break;
                    case 2:
                        data = {
                            "type": $.t("poll_creation"),
                            "name": transaction.attachment.name,
                            "description": transaction.attachment.description,
                            "finish_height": transaction.attachment.finishHeight,
                            "min_number_of_options": transaction.attachment.minNumberOfOptions,
                            "max_number_of_options": transaction.attachment.maxNumberOfOptions,
                            "min_range_value": transaction.attachment.minRangeValue,
                            "max_range_value": transaction.attachment.maxRangeValue,
                            "min_balance": transaction.attachment.minBalance,
                            "min_balance_model": transaction.attachment.minBalanceModel
                        };

                        if (transaction.attachment.votingModel == -1) {
                            data["voting_model"] = $.t("vote_by_none");
                        } else if (transaction.attachment.votingModel == 0) {
                            data["voting_model"] = $.t("vote_by_account");
                        } else if (transaction.attachment.votingModel == 1) {
                            data["voting_model"] = $.t("vote_by_balance");
                        } else if (transaction.attachment.votingModel == 2) {
                            data["voting_model"] = $.t("vote_by_asset");
                            data["asset_id"] = transaction.attachment.holding;
                        } else if (transaction.attachment.votingModel == 3) {
                            data["voting_model"] = $.t("vote_by_currency");
                            data["currency_id"] = transaction.attachment.holding;
                        } else if (transaction.attachment.votingModel == 4) {
                            data["voting_model"] = $.t("vote_by_transaction");
                        } else if (transaction.attachment.votingModel == 5) {
                            data["voting_model"] = $.t("vote_by_hash");
                        } else {
                            data["voting_model"] = transaction.attachment.votingModel;
                        }


                        for (i = 0; i < transaction.attachment.options.length; i++) {
                            data["option_" + i] = transaction.attachment.options[i];
                        }

                        if (transaction.sender != NRS.account) {
                            data["sender"] = NRS.getAccountTitle(transaction, "sender");
                        }

                        data["sender"] = transaction.senderRS ? transaction.senderRS : transaction.sender;
                        infoTable.find("tbody").append(NRS.createInfoTable(data));
                        infoTable.show();

                        break;
                    case 3:
                        var vote = "";
                        var votes = transaction.attachment.vote;
                        if (votes && votes.length > 0) {
                            for (i = 0; i < votes.length; i++) {
                                if (votes[i] == -128) {
                                    vote += "N/A";
                                } else {
                                    vote += votes[i];
                                }
                                if (i < votes.length - 1) {
                                    vote += " , ";
                                }
                            }
                        }
                        data = {
                            "type": $.t("vote_casting"),
                            "poll_formatted_html": NRS.getTransactionLink(transaction.attachment.poll),
                            "vote": vote
                        };
                        data["sender"] = transaction.senderRS ? transaction.senderRS : transaction.sender;
                        infoTable.find("tbody").append(NRS.createInfoTable(data));
                        infoTable.show();

                        break;
                    case 4:
                        data = {
                            "type": $.t("hub_announcement")
                        };

                        infoTable.find("tbody").append(NRS.createInfoTable(data));
                        infoTable.show();

                        break;
                    case 5:
                        data = {
                            "type": $.t("account_info"),
                            "name": transaction.attachment.name,
                            "description": transaction.attachment.description
                        };

                        infoTable.find("tbody").append(NRS.createInfoTable(data));
                        infoTable.show();

                        break;
                    case 6:
                        var type = $.t("alias_sale");
                        if (transaction.attachment.priceNQT == "0") {
                            if (transaction.sender == transaction.recipient) {
                                type = $.t("alias_sale_cancellation");
                            } else {
                                type = $.t("alias_transfer");
                            }
                        }

                        data = {
                            "type": type,
                            "alias_name": transaction.attachment.alias
                        };

                        if (type == $.t("alias_sale")) {
                            data["price"] = transaction.attachment.priceNQT
                        }

                        if (type != $.t("alias_sale_cancellation")) {
                            data["recipient"] = transaction.recipientRS ? transaction.recipientRS : transaction.recipient;
                        }

                        data["sender"] = transaction.senderRS ? transaction.senderRS : transaction.sender;

                        if (type == $.t("alias_sale")) {
                            message = "";
                            var messageStyle = "info";

                            NRS.sendRequest("getAlias", {
                                "aliasName": transaction.attachment.alias
                            }, function (response) {
                                NRS.fetchingModalData = false;

                                if (!response.errorCode) {
                                    if (transaction.recipient != response.buyer || transaction.attachment.priceNQT != response.priceNQT) {
                                        message = $.t("alias_sale_info_outdated");
                                        messageStyle = "danger";
                                    } else if (transaction.recipient == NRS.account) {
                                        message = $.t("alias_sale_direct_offer", {
                                            "amount": NRS.formatAmount(transaction.attachment.priceNQT), "symbol": NRS.constants.COIN_SYMBOL
                                        }) + " <a href='#' data-alias='" + NRS.escapeRespStr(transaction.attachment.alias) + "' data-toggle='modal' data-target='#buy_alias_modal'>" + $.t("buy_it_q") + "</a>";
                                    } else if (typeof transaction.recipient == "undefined") {
                                        message = $.t("alias_sale_indirect_offer", {
                                            "amount": NRS.formatAmount(transaction.attachment.priceNQT), "symbol": NRS.constants.COIN_SYMBOL
                                        }) + " <a href='#' data-alias='" + NRS.escapeRespStr(transaction.attachment.alias) + "' data-toggle='modal' data-target='#buy_alias_modal'>" + $.t("buy_it_q") + "</a>";
                                    } else if (transaction.senderRS == NRS.accountRS) {
                                        if (transaction.attachment.priceNQT != "0") {
                                            message = $.t("your_alias_sale_offer") + " <a href='#' data-alias='" + NRS.escapeRespStr(transaction.attachment.alias) + "' data-toggle='modal' data-target='#cancel_alias_sale_modal'>" + $.t("cancel_sale_q") + "</a>";
                                        }
                                    } else {
                                        message = $.t("error_alias_sale_different_account");
                                    }
                                }
                            }, { isAsync: false });

                            if (message) {
                                $("#transaction_info_bottom").html("<div class='callout callout-bottom callout-" + messageStyle + "'>" + message + "</div>").show();
                            }
                        }

                        infoTable.find("tbody").append(NRS.createInfoTable(data));
                        infoTable.show();

                        break;
                    case 7:
                        data = {
                            "type": $.t("alias_buy"),
                            "alias_name": transaction.attachment.alias,
                            "price": transaction.amountNQT,
                            "recipient": transaction.recipientRS ? transaction.recipientRS : transaction.recipient,
                            "sender": transaction.senderRS ? transaction.senderRS : transaction.sender
                        };

                        infoTable.find("tbody").append(NRS.createInfoTable(data));
                        infoTable.show();

                        break;
                    case 8:
                        data = {
                            "type": $.t("alias_deletion"),
                            "alias_name": transaction.attachment.alias,
                            "sender": transaction.senderRS ? transaction.senderRS : transaction.sender
                        };

                        infoTable.find("tbody").append(NRS.createInfoTable(data));
                        infoTable.show();

                        break;
                    case 9:
                        data = {
                            "type": $.t("transaction_approval")
                        };
                        for (i = 0; i < transaction.attachment.transactionFullHashes.length; i++) {
                            var transactionBytes = converters.hexStringToByteArray(transaction.attachment.transactionFullHashes[i]);
                            var transactionId = converters.byteArrayToBigInteger(transactionBytes, 0).toString().escapeHTML();
                            data["transaction" + (i + 1) + "_formatted_html"] =
                                NRS.getTransactionLink(transactionId);
                        }

                        infoTable.find("tbody").append(NRS.createInfoTable(data));
                        infoTable.show();

                        break;
                    case 10:
                        data = {
                            "type": $.t("set_account_property"),
                            "property": transaction.attachment.property,
                            "value": transaction.attachment.value
                        };
                        infoTable.find("tbody").append(NRS.createInfoTable(data));
                        infoTable.show();

                        break;
                    case 11:
                        data = {
                            "type": $.t("delete_account_property"),
                            "property_formatted_html": NRS.getTransactionLink(transaction.attachment.property)
                        };
                        infoTable.find("tbody").append(NRS.createInfoTable(data));
                        infoTable.show();

                        break;
                    default:
                        incorrect = true;
                        break;
                }
            } else if (transaction.type == 4) {
                switch (transaction.subtype) {
                    case 0:
                        data = {
                            "type": $.t("balance_leasing"),
                            "period": transaction.attachment.period,
                            "lessee": transaction.recipientRS ? transaction.recipientRS : transaction.recipient
                        };

                        infoTable.find("tbody").append(NRS.createInfoTable(data));
                        infoTable.show();

                        break;
                    case 1:
                        data = {
                            "type": $.t("phasing_only")
                        };

                        NRS.getPhasingDetails(data, transaction.attachment.phasingControlParams);

                        infoTable.find("tbody").append(NRS.createInfoTable(data));
                        infoTable.show();

                        break;

                    default:
                        incorrect = true;
                        break;
                }
            }

            if (incorrect) {
                $.growl($.t("error_unknown_transaction_type"), {
                    "type": "danger"
                });

                NRS.fetchingModalData = false;
                return;
            }

            if (!async) {
                if (!isModalVisible) {
                    $("#transaction_info_modal").modal("show");
                }
                NRS.fetchingModalData = false;
            }
        } catch (e) {
            NRS.fetchingModalData = false;
            throw e;
        }
    };

    function listPublicKeys(publicKeys) {
        var rows = "<table class='table table-striped'><tbody>";
        for (var i = 0; i < publicKeys.length; i++) {
            var recipientPublicKey = publicKeys[i];
            var recipientAccount = {accountRS: NRS.getAccountIdFromPublicKey(recipientPublicKey, true)};
            rows += "<tr>" +
                "<td>" + NRS.getAccountLink(recipientAccount, "account") + "<td>" +
                "</tr>";
        }
        rows += "</tbody></table>";
        return rows;
    }

    $(document).on("click", ".approve_transaction_btn", function (e) {
        e.preventDefault();
        var approveTransactionModal = $('#approve_transaction_modal');
        approveTransactionModal.find('.at_transaction_full_hash_display').text($(this).data("transaction"));
        approveTransactionModal.find('.at_transaction_timestamp').text(NRS.formatTimestamp($(this).data("timestamp")));
        $("#approve_transaction_button").data("transaction", $(this).data("transaction"));
        approveTransactionModal.find('#at_transaction_full_hash').val($(this).data("fullhash"));

        var mbFormatted = $(this).data("minBalanceFormatted");
        var minBalanceWarning = $('#at_min_balance_warning');
        if (mbFormatted && mbFormatted != "") {
            minBalanceWarning.find('.at_min_balance_amount').html(mbFormatted);
            minBalanceWarning.show();
        } else {
            minBalanceWarning.hide();
        }
        var revealSecretDiv = $("#at_revealed_secret_div");
        if ($(this).data("votingmodel") == NRS.constants.VOTING_MODELS.HASH) {
            revealSecretDiv.show();
        } else {
            revealSecretDiv.hide();
        }
    });

    $("#approve_transaction_button").on("click", function () {
        $('.tr_transaction_' + $(this).data("transaction") + ':visible .approve_transaction_btn').attr('disabled', true);
    });

    $("#transaction_info_modal").on("hide.bs.modal", function () {
        NRS.removeDecryptionForm($(this));
        $("#transaction_info_output_bottom, #transaction_info_output_top, #transaction_info_bottom").html("").hide();
    });

    return NRS;
}(NRS || {}, jQuery));
